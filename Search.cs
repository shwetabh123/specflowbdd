﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace SpecFlowBDD
{
    [Binding]




    public class Search
    {


        public IWebDriver driver;

        [Given(@"Launch Chrome")]
        public void GivenLaunchChrome()
        {
            driver = new ChromeDriver(@"C:\Users\Admin\CWorkspace\AutomationFramework3\SpecFlowBDD\driver");


            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(1);


        }

    

        [When(@"Navigate to Google")]
        public void WhenNavigateToGoogle()
        {
            driver.Navigate().GoToUrl("https://www.google.com/");

              Thread.Sleep(19000);
        }


       
        [When(@"Navigate to Gartner")]
        public void WhenNavigateToGartner()
        {
            driver.Navigate().GoToUrl("http://stg-surveys.cebglobal.com/Pulse");

          //  driver.Manage().Window.Maximize();

            Thread.Sleep(49000);
        }



        [When(@"I provide the ""(.*)"", and ""(.*)""")]
        public void WhenIProvideTheAnd(string user, string password)
        {

            driver.FindElement(By.XPath("//*[@id='j_username']")).Clear();
            Thread.Sleep(19000);

            driver.FindElement(By.XPath("//*[@id='j_username']")).SendKeys(user);

            // driver.FindElement(By.XPath("//*[@id='j_username']")).SendKeys(TestContextInstance.DataRow[""].ToString);


            Thread.Sleep(19000);

            driver.FindElement(By.XPath("//*[@id='j_password']")).Clear();
            Thread.Sleep(19000);



            driver.FindElement(By.XPath("//*[@id='j_password']")).SendKeys(password);


            Thread.Sleep(19000);
        }


       


        [Then(@"Google Title should be displayed")]
        public void ThenGoogleTitleShouldBeDisplayed()
        {
            String title = driver.Title;
            Console.WriteLine("Title of webpage is " + title);

            driver.Quit();
        }


        [Then(@"ML Title should be displayed")]
        public void ThenMLTitleShouldBeDisplayed()
        {
            String title = driver.Title;
            Console.WriteLine("Title of webpage is " + title);

            driver.Quit();
        }












    }
}
