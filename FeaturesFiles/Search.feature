﻿Feature: Title


	
Scenario Outline: Validate Title of Gartner
	Given Launch Chrome
    When Navigate to Gartner
	And I provide the "<user>", and "<password>"
	Then ML Title should be displayed

	Examples:

	| user                            | password     |
	| shwetabh.srivastava@gartner.com | Password@123 |
	| kalpana.kkvp@gartner.com        | Password@123 |
	| gayathri@gartner.com            | Password@123 |
	| aditya@gartner.com              | Password@123 |




	@ignore
	Scenario: Validate Title of Google
	Given Launch Chrome
    When Navigate to Google
	Then Google Title should be displayed

